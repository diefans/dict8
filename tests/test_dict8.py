import pytest


@pytest.mark.parametrize(
    "a,b,mappable",
    [
        ({}, {}, True),
        (None, {}, False),
        (1, {}, False),
        (1.1, {}, False),
        ("str", {}, False),
        (True, {}, False),
        ((), {}, False),
        ([], {}, False),
    ],
)
def test_merger_call_unmappable(a, b, mappable, dict8):
    m = dict8.Merger(lambda a, b, path: ..., mapper=[dict8.Mapping])

    if mappable:
        m(a, b)
    else:
        with pytest.raises(dict8.Unmappable) as e:
            m(a, b)
        assert a == e.value.args[0]


def test_merger_callback(dict8):
    def cb(a, b, path):
        return a.value if not b else b.value

    m = dict8.Merger(cb, mapper=[dict8.Mapping])

    r = m({1: 2, 2: 3}, {2: 4, 3: 5})

    assert r == {1: 2, 2: 4, 3: 5}


def test_merger_callback_kwargs(dict8):
    def cb(a, b, path, /, foo=None):
        return foo if not b else b.value

    m = dict8.Merger(cb, mapper=[dict8.Mapping])

    r = m({1: 2, 2: 3}, {2: 4, 3: 5}, foo="bar")

    assert r == {1: "bar", 2: 4, 3: 5}


def test_merger_callback_nested(dict8):
    def cb(a, b, path):
        return a.value if not b else b.value

    m = dict8.Merger(cb, mapper=[dict8.Mapping])

    r = m(
        {
            1: 2,
            2: {
                3: 4,
                4: 5,
            },
        },
        {
            2: {
                4: 6,
                6: 7,
            },
            3: 5,
        },
    )

    assert r == {
        1: 2,
        2: {
            3: 4,
            4: 6,
            6: 7,
        },
        3: 5,
    }


def test_merger_dataclass(mocker, dataclass, dict8):
    import typing as t

    # to enable walk-through
    def with_missing(kv: dict8.KeyValues):
        {k: v for k, v in kv}
        return dict8.missing

    mocker.patch.object(
        dict8.WithFields, "create_from_key_values", side_effect=with_missing
    )

    @dataclass
    class Bar:
        bim: str
        bam: t.Union[t.Optional[int], t.Union[str, t.Tuple[int]]] = None

    @dataclass
    class Foo:
        baz: str
        bar: t.Optional[Bar]

    cb = mocker.Mock(return_value=dict8.missing)

    m = dict8.Merger(cb, mapper=[dict8.Mapping, dict8.Dataclass, dict8.Attrs])

    r = m(
        Foo,
        {
            "bar": {
                "bim": "bam",
                "bam": 1,
            },
            "baz": "str",
        },
    )
    assert cb.has_calls(
        [
            (
                dict8.Item(value=dict8.missing, key="bam", type=t.Tuple[int]),
                dict8.Item(value=1, key="bam", type=int),
                ("bar", "bam"),
            ),
            (
                dict8.Item(value=dict8.missing, key="bam", type=str),
                dict8.Item(value=1, key="bam", type=int),
                ("bar", "bam"),
            ),
            (
                dict8.Item(value=dict8.missing, key="bam", type=int),
                dict8.Item(value=1, key="bam", type=int),
                ("bar", "bam"),
            ),
            (
                dict8.Item(value=dict8.missing, key="bam", type=dict8.NoneType),
                dict8.Item(value=1, key="bam", type=int),
                ("bar", "bam"),
            ),
            (
                dict8.Item(value=dict8.missing, key="bim", type=str),
                dict8.Item(value="bam", key="bim", type=str),
                ("bar", "bim"),
            ),
            (
                dict8.Item(value=dict8.missing, key="baz", type=str),
                dict8.Item(value="str", key="baz", type=str),
                ("baz",),
            ),
        ]
    )
    assert r is dict8.missing
