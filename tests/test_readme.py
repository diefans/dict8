def test_readme_merge(dict8, dataclass):
    import typing as t

    @dataclass
    class Foo:
        my_value: int
        some: str = "default"

    @dataclass
    class Bar:
        foo: Foo
        baz: t.Optional[int] = None

    bar = dict8.merge(Bar, {"foo": {"my_value": 123}})

    assert bar == Bar(foo=Foo(my_value=123, some="default"), baz=None)
