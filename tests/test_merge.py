import pytest


def test_merge_merger_instance(dict8):
    assert isinstance(dict8.merge, dict8.Merger)


def test_merge_nothing_new(dict8):
    result = dict8.merge(
        {1: 2, 2: 3},
        {2: 5, 3: 4},
        nothing_new=True,
        keep_type=False,
    )
    assert result == {1: 2, 2: 5}


def test_merge_override_no_new(dict8):
    def override(a, b, path):
        if a == 3:
            return dict8.Item(value=999, key=a.key)
        if b == 4:
            return dict8.Item(value=777, key=a.key)
        return b

    result = dict8.merge(
        {1: 2, 2: 3},
        {2: 5, 3: 4},
        nothing_new=True,
        keep_type=False,
        override=override,
    )
    assert result == {1: 2, 2: 999}


def test_merge_override_new(dict8):
    def override(a, b, path):
        if a == 3:
            return dict8.Item(value=999, key=a.key)
        if b == 4:
            return dict8.Item(value=777, key=a.key)
        return b

    result = dict8.merge(
        {1: 2, 2: 3},
        {2: 5, 3: 4},
        nothing_new=False,
        override=override,
    )
    assert result == {1: 2, 2: 999, 3: 777}


def test_merge_override_not_keep_type(dict8):
    def override(a, b, path):
        if a.value == 3:
            return a.replace(value="foo", type=dict8.missing)
        if b.value == 4:
            return a.replace(value="bar", type=dict8.missing)
        return b

    result = dict8.merge(
        {1: 2, 2: 3},
        {2: 5, 3: 4},
        keep_type=False,
        override=override,
    )
    assert result == {1: 2, 2: "foo", 3: "bar"}


def test_merge_remove_old(dict8):
    result = dict8.merge(
        {1: 2, 2: 3},
        {2: 5, 3: 4},
        nothing_new=False,
        remove_old=True,
    )
    assert result == {2: 5, 3: 4}


def test_merge_strict_dataclass_fields_type_match(dict8, dataclass):
    @dataclass
    class Foo:
        bar: str

    result = dict8.merge(
        Foo,
        {"bar": "5", 3: 4},
        # keep_type=True,
    )
    assert result == Foo("5")


def test_merge_strict_dataclass_fields_type_wrong(dict8, dataclass):
    @dataclass
    class Foo:
        bar: str

    with pytest.raises(dict8.Unmappable) as e:
        dict8.merge(
            Foo,
            {"bar": 5, 3: 4},
            keep_type=True,
        )
