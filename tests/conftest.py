import pytest


@pytest.fixture
def dict8():
    import dict8

    return dict8


@pytest.fixture(
    params=[
        "dataclass",
        "attrs",
    ],
)
def dataclass(request):
    if request.param == "attrs":
        import functools as ft
        import attr

        return ft.partial(attr.s, auto_attribs=True)
    else:
        import dataclasses as dc

        return dc.dataclass
