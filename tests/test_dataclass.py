def testdataclass_mapper(dataclass, dict8):
    @dataclass
    class Foo:
        bar: str

    @dataclass
    class Bar:
        foo: Foo
        baz: str = "default"

    m = dict8.Merger(func=None, mapper=[dict8.Dataclass, dict8.Attrs])

    assert m.map(Bar).keys() == {"foo", "baz"}


def test_dataclass_merge_cls(dataclass, dict8):
    import typing as t

    @dataclass
    class Bar:
        bar: t.Dict

    @dataclass
    class Foo:
        foo: Bar
        string: str = "foobar"

    @dict8.ion(dict8.Mapping, dict8.Dataclass, dict8.Attrs)
    def merge(a, b, path=(), /, **kw):
        return a.value if b.value_is_missing else b.value

    result = merge(
        Foo,
        {
            "string": "bimbam",
            "foo": {
                "bar": {1: 2},
            },
            "missing": "should be skipped",
        },
    )

    assert result == Foo(string="bimbam", foo=Bar(bar={1: 2}))


def test_dataclass_merge_inst(dataclass, dict8):
    import typing as t

    @dataclass
    class Bar:
        bar: t.Dict

    @dataclass
    class Foo:
        foo: Bar
        str: str = "foobar"

    result = dict8.merge(
        Foo(str="foobar", foo=Bar(bar={1: {2: 3, 5: 6}})),
        {"str": "bimbam", "foo": {"bar": {1: {2: 1, 3: 4}}}},
    )

    assert result == Foo(str="bimbam", foo=Bar(bar={1: {2: 1, 3: 4, 5: 6}}))


def test_dataclass_generic(dict8, dataclass):
    import typing as t

    @dataclass
    class Bar:
        n: int

    @dataclass
    class Foo:
        bar: t.List[Bar]

    assert dict8.merge(Foo, {"bar": [{"n": 1}, {"n": 2}]}) == Foo(bar=[Bar(1), Bar(2)])


def test_dataclass_generic_instance(dict8, dataclass):
    import typing as t

    @dataclass
    class Bar:
        n: int

    @dataclass
    class Foo:
        bar: t.List[Bar]

    r = dict8.merge(Foo(bar=[Bar(1)]), {"bar": [{"n": 1}, {"n": 2}]})

    assert r == Foo(bar=[Bar(1), {"n": 2}])


def test_dataclass_type_in_dict(dict8, dataclass):
    @dataclass
    class Foo:
        n: int

    r = dict8.merge({"foo": Foo}, {"foo": {"n": 1}})
    assert r == {"foo": Foo(1)}


def test_dataclass_type_list_in_dict(dict8, dataclass):
    import typing as t

    @dataclass
    class Foo:
        n: int

    r = dict8.merge({"foo": t.List[Foo]}, {"foo": [{"n": 1}]})
    assert r == {"foo": [Foo(1)]}


def test_dataclass_extra_fields(dict8, dataclass):
    import typing as t
    @dataclass
    class Foo:
        bar: str

    r = dict8.merge(Foo, {"bar": "string", "extra": 123})
    assert r == Foo(bar="string")

